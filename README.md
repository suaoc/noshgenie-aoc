
# Yeoman

The web interface is being managed using Yeoman.

"[Yeoman](http://yeoman.io/index.html) is a robust and opinionated set of tools, libraries, and a workflow that can help developers quickly build beautiful, compelling web apps."


For installation details check out the website.

The two important commands are

1. 'yeoman server' 

In the root directory of the project this command will start the in built server for local development.

2. 'yeoman build'

This command generates a clean build of the source for deployment. It concatenates and minimizes script files, style sheets, images and  performs a number of other optimizations. The build is put into the build folder.



# Deployment

The web interface is hosted on S3. It can be accessed through www.noshgenie.com.

To update, do 'yeoman build' and then upload all the files in the 'dist' folder to www.noshgenie.com S3 bucket.


# Susy-off-canvas layout

The interface is using this sass plugin for a responsive layout:

http://oddbird.net/2012/11/27/susy-off-canvas/
