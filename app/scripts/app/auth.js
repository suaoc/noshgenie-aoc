Genie.Auth = {};

Genie.Auth.loggedIn = false; // if user is logged in from a previous session

Genie.Auth.loginToGenie = function(accessToken) {
  $.ajax({
    url: Genie.Router.router.baseUrl + "/profiles/auth/facebook/",
    type:"GET",
    data: {"access_token": accessToken},
    contentType:"application/json",
    dataType:"json",
    success: function(data){
      console.log(data);
    }
  });
}

Genie.Auth.loginStatusHandler = function(response) {
  if (response.status === 'connected') {
    Genie.Auth.loginToGenie(response.authResponse.accessToken);
    Genie.Auth.loggedIn = true;
  } else if (response.status === 'not_authorized') {
    // not authorized
  } else {
    // not_logged_in
    // and we dont want to ask user to login on web page load
  }
  Genie.fbWidget = new Genie.Widgets.FBWidget();
  Genie.fbWidget.render();
};

window.fbAsyncInit = function() {
  FB.init({
    appId      : '133766870094240', // App ID
    channelUrl : Genie.Router.router.baseUrl + '/channel.html', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });
  
  FB.getLoginStatus(Genie.Auth.loginStatusHandler);

  FB.Event.subscribe('auth.authResponseChange', function(response){
    Genie.placeCollection.updateSelectedChild();
    if(response.status === 'connected'){
      //update 
      Genie.Auth.loggedIn = true;
    }else if(response.status === 'not_authorized'){
    }else{
      Genie.Auth.loggedIn = false;
    }
  });
};

(function(d){
 var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement('script'); js.id = id; js.async = true;
 js.src = "//connect.facebook.net/en_US/all.js";
 ref.parentNode.insertBefore(js, ref);
}(document));


Genie.Auth.checkLogin = function(cb) {
  //FB.getLoginStatus(Genie.Auth.loginStatusHandler);
  FB.getLoginStatus(cb);
};
