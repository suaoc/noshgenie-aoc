
Genie.Places = { };

// Models
Genie.Places.PlaceModel = Backbone.Model.extend({
  isSelected: false,
  reRender: false,

  select: function(){
    this.set({isSelected: true});
  },

  parse: function(response) {
    response.menus = new Genie.Menus.MenuCollection(response.menus, { parse: true });
    response.opinions = new Genie.Opinions.OpinionCollection(response.opinions, { parse:true });
    response.photos = new Genie.Photos.PhotoCollection(response.photos, { parse:true });
    return response;
  },
});


// Collections
Genie.Places.PlaceCollection = Backbone.Collection.extend({
  model: Genie.Places.PlaceModel,
  title: '',
  query: '',
  isEmpty: false,

  initialize: function() {
    _.bindAll(this, 'resetMeta');
    this.on('change:isSelected', this.onSelectedChanged, this);
    this.nextUrl = null;
  },

  parse: function(response) {
    this.nextUrl = response.meta['next'];
    this.prevUrl = response.meta['previous'];
    this.totalPlacesCount = response.meta['total_count'];
    return response.objects;
  },

  onSelectedChanged: function(model) {
    this.each(function(model) {
      if (model.get('isSelected') === true && !model.hasChanged('isSelected')) {
        model.set({isSelected: false});
      }
    });
  },

  updateSelectedChild: function(){
    this.each(function(model) {
      if (model.get('isSelected') === true) {
        model.set({'reRender': true});
        model.trigger('change:reRender')
        return;
      }
    });  
  },

  url: function() {
    if(this.nextUrl){
      return Genie.Router.router.baseUrl + this.nextUrl;
    }
    url = Genie.Router.router.baseUrl + '/api/v1/places/?format=json&menus=minimal&limit=5&opinions_limit=3&photos=true';
    if(this.query){
      url += '&search_query=' + this.query;
      this.query = '';
    }
    console.log('places url: ' + url);
    return url;
  },

  resetMeta: function() {
    this.nextUrl = null;
  },
  searchRequested: function(q){
    this.nextUrl = null;
    this.reset();
    this.query = q;
  }
});


// Views
Genie.Places.PlacesView = Backbone.View.extend({
  tagName: 'div',
  className: 'places',

  template: _.template(
    "<span id='list-hdr'></span>\
    <div class='places-list'></div>\
    <div class='show-more-button'>\
      <span class='icon'></span>\
      <span class='title'>Show More</span>\
    </div>"
    ),

  events: {
    'click .show-more-button': 'loadMore',
  },

  initialize: function() {
    _.bindAll(this, 'render', 'loadMore', 'addPlace', 'updateMoreButton', 'showMoreButton', 'hideMoreButton', 'animateMoreButton', 'stopAnimatingMoreButton');
    this.collection.bind('reset', this.render);
    this.collection.on('add', this.addPlace);
    this.collection.on('change:isEmpty', this.noResultsFound, this)
    this.isLoading = false;
  },

  loadMore: function(){
    this.isLoading = true;
    this.$('.places-list').append('<div class="page-separator"></div>');
    this.animateMoreButton();
    var view = this;
    this.collection.fetch({
      success: function(collection){
        view.isLoading = false;
        view.stopAnimatingMoreButton();
      },
      add: true,
    });
  },

  addPlace: function(model, collection, options) {
    var view = new Genie.Places.PlaceItemView({ model: model });
    this.$('.places-list').append(view.render());
  },

  render: function() {
    $(this.el).html(this.template);
    if(this.collection.title){
      this.$('#list-hdr').html(this.collection.title);
      this.$('#list-hdr').addClass('nav-header');
    }
    this.collection.forEach(function(model) {
      view = new Genie.Places.PlaceItemView({ model: model });
      this.$('.places-list').append(view.render());
    });
    this.updateMoreButton();
    this.collection.title = '';
  },
  noResultsFound: function(isEmpty){
    if(this.collection.isEmpty){
      this.$('#list-hdr').html('No results found!');
      this.$('#list-hdr').addClass('nav-header');
    }
  },

  reset: function(){
    $(this.el).html("");
  },

  updateMoreButton: function() {
    if (this.collection && this.collection.nextUrl) {
      this.showMoreButton();
    } else {
      this.hideMoreButton();
    } 
  },

  showMoreButton: function() {
    this.$('.show-more-button').show();
  },

  hideMoreButton: function() {
    this.$('.show-more-button').hide();
  },

  stopAnimatingMoreButton: function() {
    this.$('.show-more-button .icon').spin(false);
    this.$('.show-more-button .icon').css({ 'min-width': '0px' });
    this.$('.show-more-button .title').html('Show More'); 
  },    

  animateMoreButton: function() {
    this.$('.show-more-button .icon').spin({'length': 4, 'lines': 7, 'radius': 3, 'width': 3,});
    this.$('.show-more-button .icon').css({ 'min-width': '20px' });
    this.$('.show-more-button .title').html('fetching...');
  },
});

Genie.Places.PlaceItemView = Backbone.View.extend({
  tagName: 'div',
  className: 'place-item',
 template: _.template(
      "<div class='place-info'>\
        <div class='restaurant-name'><%= restaurant_name%></div>\
        <div class='checkins'>\
            <span class='checkins-count'><%= checkins %> checkins</span>\
        </div>\
        <div class='rating'>\
            <% for (var i = 1; i <= 5; i++) {\
                if (i <= rating) { %>\
                    <span>★</span>\
                <%} else { %>\
                    <span>☆</span>\
                <% } %>\
            <% };%>\
        </div>\
        <div class='address'><%= address_1 %>, <%= city_town %></div>\
        <div class='phone'> Phone: <%= phone %> </div>\
        <% if (brief_description) { %>\
          <div class='description'> Description: <%= brief_description %></div>\
        <% } %>\
      </div>\
      "
    ),

  events: {
    "click .place-info": "showDetails",
    "click #map-marker": "showMap",
  },

  initialize: function() {
    _.bindAll(this, 'render', 'showDetails', 'onSelectedChanged');
    this.model.bind("change:isSelected", this.onSelectedChanged);
  },

  onSelectedChanged: function() {
    console.log(this.model.get('isSelected'));  
    if (this.model.get('isSelected') === true) {
      this.$('.place-info').animate({ backgroundColor: "#C2D1F1" }, 500);
    }else {
      this.$('.place-info').animate({ backgroundColor: "rgba(0, 0, 0, 0)" }, 500);
    }
  },
  render: function(model) {
    //console.log('calling render placeItemView');
    $(this.el).append(this.template(this.model.toJSON()));
    if(!(this.model.get('latitude') || this.model.get('longitude'))){
      $('#map-marker', this.el).remove();
    }
    return $(this.el);
  },

  showDetails: function() {
    this.model.select();
    Genie.Router.router.navigate("browse/"+this.model.get("id"), {trigger: true, replace: true});
  },

  showMap: function(){
    lat = this.model.get('latitude');
    lon = this.model.get('longitude');
    if(lat & lon){
      mapsUrl = 'http://maps.google.com/?q=' + lat + ',' + lon;
      window.open(mapsUrl, '_blank');
      window.focus();
    }else{
      console.log("lat long is null");
    }
  },
});