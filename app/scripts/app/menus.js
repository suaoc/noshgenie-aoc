Genie.Menus = { };

// Models
Genie.Menus.SizeModel = Backbone.Model.extend({
    parse: function(response) {
        return response;
    },
});

Genie.Menus.ItemModel = Backbone.Model.extend({
    parse: function(response) {
        response.sizes = new Genie.Menus.SizeCollection(response.sizes, {
            parse: true
        });
        return response;
    },
});

Genie.Menus.GroupModel = Backbone.Model.extend({
    parse: function(response) {
        response.items = new Genie.Menus.ItemCollection(response.items, {
            parse: true
        });
        return response;
    },
});

Genie.Menus.MenuModel = Backbone.Model.extend({
    initialize: function() {},
    parse: function(response) {
        response.groups = new Genie.Menus.GroupCollection(response.groups, {
            parse: true
        });
        return response;
    },
});


// Collections
Genie.Menus.SizeCollection = Backbone.Collection.extend({
    model: Genie.Menus.SizeModel,
});

Genie.Menus.ItemCollection = Backbone.Collection.extend({
    model: Genie.Menus.ItemModel,
});

Genie.Menus.GroupCollection = Backbone.Collection.extend({
    model: Genie.Menus.GroupModel,
});

Genie.Menus.MenuCollection = Backbone.Collection.extend({
    model: Genie.Menus.MenuModel,
    parse: function(response) {
        return response.menus;
    },

    url: function() {
        return 'http://severe-wind-1686.herokuapp.com/api/v1/places/16/?format=json&menus=minimal';
    }
});

// Views
Genie.Menus.MenuListView = Backbone.View.extend({
    tagName: 'div',
    className: 'menu-list',
    template: _.template(""),

    initialize: function() {
        _.bindAll(this, 'render');
    },

    render: function() {
        var menu_list_element = this.el;
        $(this.el).html(this.template());
        this.collection.forEach(function(m) {
            view = new Genie.Menus.MenuItemView({
                model: m
            });
            $(menu_list_element).append(view.render());
        });
        return $(this.el);
    }
});

Genie.Menus.MenuItemView = Backbone.View.extend({
  tagName: 'div',
  className: 'menu',
  template: _.template('<span class="menu-name"><%= name %></span>'),

  render: function() {
    $(this.el).append(this.template(this.model.toJSON()));
    view = new Genie.Menus.GroupListView({collection: this.model.get('groups')});
    return $(this.el).append(view.render());
  }
});

Genie.Menus.GroupListView = Backbone.View.extend({
  tagName: 'div',
  className: 'group-list',
  id: 'my-group-list',
  template: _.template(''),

  render: function(){
    var group_list_element = this.el;
    this.collection.forEach(function(m){
      view = new Genie.Menus.GroupItemView({model: m});
      $(group_list_element).append(view.render());
    });
    return $(this.el);
  },
});

Genie.Menus.GroupItemView = Backbone.View.extend({
  tagName: 'div',
  className: 'group',
  template: _.template('<span class="group-name"><%= name %></span>'),

  render: function(){
    $(this.el).append(this.template(this.model.toJSON()));
    view  = new Genie.Menus.ItemListView({collection: this.model.get('items')});
    $(this.el).append(view.render());
    return $(this.el);
  },
});

Genie.Menus.ItemListView = Backbone.View.extend({
  tagName: 'div',
  className: 'item-list',
  template: _.template(''),

  render: function(){
    var item_list_element = this.el;
    this.collection.forEach(function(m){
      view = new Genie.Menus.ItemItemView({model: m});
      $(item_list_element).append(view.render());
    });
    return $(this.el);
  },
});

Genie.Menus.ItemItemView = Backbone.View.extend({
  tagName: 'span',
  className: 'item',
  template:  _.template('<span class="item-name"><%= name %></span><span class="size-price"></span>'),

  render: function(){
    $(this.el).append(this.template(this.model.toJSON()));
    var sizes = this.model.get('sizes');
    
    if (sizes.length == 1 && sizes.at(0).get('name') == 'Standard') {
        this.$('.size-price').html(sizes.at(0).get('price'));
        return $(this.el);
    } else {
        view = new Genie.Menus.SizeListView({collection: sizes});
        $(this.el).append(view.render());
        return $(this.el);
    }
  },  
});

Genie.Menus.SizeListView = Backbone.View.extend({
  tagName: 'span',
  className: 'size-list',
  template:  _.template(''),

  render: function(){
    var size_list_element = this.el;
    this.collection.forEach(function(m){
      view = new Genie.Menus.SizeItemView({model: m});
      $(size_list_element).append(view.render());
    });
    return $(this.el);
  },
});

Genie.Menus.SizeItemView = Backbone.View.extend({
  tagName: 'span',
  className: 'size',
  template: _.template('\
                    <span class="size-name"><%= name %></span>\
                    <span class="size-price"><%= price %></span>\
                    '),

  render: function(){
    return $(this.el).append(this.template(this.model.toJSON()));
  },
});
