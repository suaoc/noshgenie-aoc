Genie.Opinions = {};

// Models
Genie.Opinions.AuthorModel = Backbone.Model.extend({
});

Genie.Opinions.OpinionModel = Backbone.Model.extend({
    parse: function(response) {
        response.author = new Genie.Opinions.AuthorModel(response.author);
        return response;
    },
    
    url: function() {
        var root = this.urlRoot();
        if (this.isNew()) return root;
        return root + this.id + '/';
    },
    urlRoot: function() {
        return Genie.Router.router.baseUrl +  '/api/v1/opinions/';
    },
    
});


// Collections
Genie.Opinions.OpinionCollection = Backbone.Collection.extend({
    model: Genie.Opinions.OpinionModel,
    parse: function(response) {
        return response.objects;
    },

    url: function() {
        return Genie.Router.router.baseUrl + '/api/v1/places/16/opinions/?format=json&opinions_limit=2';
    }
});


// Views
Genie.Opinions.OpinionsView = Backbone.View.extend({
    tagName: 'div',
    className: 'opinion-list',
    template: _.template(""),

    initialize: function() {
        _.bindAll(this, 'render');
        this.collection.bind('reset', this.render);
        this.collection.bind('change', this.render);
    },

    render: function() {
        $(this.el).html(this.template);
        var opinions_list_element = this.el;
        this.collection.forEach(function(m) {
            view = new Genie.Opinions.OpinionItemView({
                model: m
            });
            $(opinions_list_element).append(view.render());
        });
        $.expander.defaults.slicePoint = 150;
        $('.expandable').expander();

        return $(this.el);
    }
});

Genie.Opinions.OpinionItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'opinion-item',
    template: _.template($('#opinions-template').html()),

    initialize: function() {
        _.bindAll(this, 'render', 'like', 'incrementLikeCount');
    },
    
    events: {
       "click .like-button": "like",
    },

    render: function() {
        author = new Genie.Opinions.AuthorView({
            model: this.model.get('author')
        });
        $(this.el).append(this.template(this.model.toJSON()));
        this.$(".author").html(author.render());
        return $(this.el);
    },
    
    incrementLikeCount: function() {
        var like_count = this.model.get('likes') || 0;
        var new_like_count = like_count + 1;
        this.$('.likes-count').html(new_like_count + ' likes');
    },
    
    like: function() {
        var view = this;
        var done = false;
        this.$('.like-button .icon').animate({
            'min-width': '20px',
        }, 50, function() {
           if (!done) view.$('.like-button .icon').spin({'length': 3, 'lines': 7, 'radius': 3, 'width': 3,});
        });
      
        var like = new Genie.Likes.LikeModel({ opinion: this.model.get('resource_uri') });
        like.save({}, {
            error: function(model, response) {
                done = true;
                view.$('.like-button .icon').spin(false);
                view.$('.like-button .icon').css({ 'min-width': '0px' })
                view.$('.like-button .title').html('Like');
            },
            success: function(model, response) {
                done = true;
                view.$('.like-button .icon').spin(false);
                view.$('.like-button .icon').css({ 'min-width': '0px' })
                view.$('.like-button .icon').html('✓');
                view.$('.like-button .title').html('Liked');
                view.incrementLikeCount();
            },
        });
    },
});

Genie.Opinions.AuthorView = Backbone.View.extend({
    template: _.template('<span class="author-name"><%= name %></span>'),

    render: function() {
        return $(this.el)
            .append(this.template(this.model.toJSON()));
    },
});
