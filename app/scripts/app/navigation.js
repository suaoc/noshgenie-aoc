Genie.Navigation = {};

Genie.Navigation.NavBarView = Backbone.View.extend({
    el: $('#navigation'),
    template: _.template(
        '<ul class="nav nav-tabs nav-stacked">\
            <li class="active"><a href="#">Home</a></li>\
            <li><a href="#">Popular Restaurants</a></li>\
            <li><a href="#">Featured Restaurants</a></li>\
            <li><a href="#">Featured Reviews</a></li>\
            <li><a href="#about">About</a></li>\
            <li><a href="#">Contact</a></li>\
        </ul>'
        ),
    
    events: {
    },

    render: function(){
        return $(this.el).html(this.template());
    }
});

Genie.Navigation.SearchBarView = Backbone.View.extend({
    el: $('.search-bar'),
/*    template: _.template(
    '<div class="navbar-search search-field">\
        <input type="text" class="search-query search-field" id="search-input" placeholder="Search"/>\
        <em class="icon-search"></em>\
      </div>'
    ),*/
    template: _.template("<div class='input-append'>\
        <input type='text' class='search-field'  id='search-input' placeholder='Search Restaurants'/>\
        <button class='btn add-on'><i class='icon-search'></i></button>\
        </div>"),
    
    initialize: function(){
      _.bindAll(this, 'render', 'onKeyPress');
    },
    
    events: {
        'keyup .search-field': 'onKeyPress',
        'click .btn': 'btnClicked',
    },

    render: function(){
        $(this.el).html(this.template());
        //this.initSelect2(this);
        return $(this.el);
    },

    btnClicked: function(e){
        q = this.$('#search-input').val();
        if(!q){
            return;
        }        
        Genie.Router.router.navigate('search/'+ q, {trigger: true, replace: true}); 
    },

    onKeyPress: function(e){
        q = this.$('#search-input').val();
        if(!q){
            return;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            Genie.Router.router.navigate('search/' + q, {trigger: true, replace: true});
            // if using select2, $('#search-input').val() returns id of the place
            //Genie.Router.router.navigate('browse/' + q, {trigger: true, replace: true});
        }
    },

    /*********** Select2 functions *************/
    initSelect2: function(view) {
        this.$('#search-input').select2({
            minimumInputLength: 3,
            // instead of writing the function to execute the request we use Select2's convenient helper
            ajax: {
                url: "http://severe-wind-1686.herokuapp.com/api/v1/places/?format=json",
                dataType: 'json',
                data: function (term, page) {
                    return {
                      search_query: term, // search term
                      limit: 10,
                    };
                },
                results: function (data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {results: data.objects};
                }
            },
            formatResult: view.placeFormatResult,
            formatSelection: view.placeFormatSelection, 
            // apply css that makes the dropdown taller
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) { 
                 // we do not want to escape markup since we are displaying html in results,
                 return m; 
             },
        });
    },
    placeFormatResult: function (r) {
        var markup = "<table class='movie-result'><tr>";
        if(r){
          markup += "<div><span>" + r.restaurant_name + "</span><br/><span><small>" +  r.city_town +  "</small></span></div>";
        }
        markup += "</td></tr></table>"
        return markup;
    },

    placeFormatSelection: function (r) {
        return r.restaurant_name;
    },
});