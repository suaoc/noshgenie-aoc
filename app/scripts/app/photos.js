Genie.Photos = {};

// Models
Genie.Photos.AuthorModel = Backbone.Model.extend({
});

var testVar; 
Genie.Photos.PhotoModel = Backbone.Model.extend({
    parse: function(response) {
        if (response.image) response.image = response.image.replace("https://", "http://");
        testVar = response;
        response.author = new Genie.Photos.AuthorModel(response.author);
        return response;
    }
});


// Collections
Genie.Photos.PhotoCollection = Backbone.Collection.extend({
    model: Genie.Photos.PhotoModel,

    parse: function(response) {
        console.log(response);
        return response.photos;
    },
});

// Views
Genie.Photos.PhotosView = Backbone.View.extend({
    //tagName: $('#inner-gallery'),
    className: 'image_carousel',
    template: _.template(
        "<div class='thumbnails'></div>\
        <a class='prev' id='prev_img' href='#''><span>prev</span></a>\
        <a class='next' id='next_img' href='#''><span>next</span></a>"
    ),

    render: function(){
        $(this.el).html(this.template());
        that = this;
        this.collection.forEach(function(m){
            piv = new Genie.Photos.PhotoItemView({ model:m });
            that.$('.thumbnails').append(piv.render());
        });

        return $(this.el);
    },
});

Genie.Photos.PhotoItemView = Backbone.View.extend({
    tagName: 'li',
    template: _.template(
        '<a id="image-prv" href="#imageModal" role="button" data-toggle="modal" class="thumbnail">\
            <img src="<%= image %>" alt="" style="width:160px;height:160px;">\
        </a>'),

    events: {
        'click #image-prv': 'showModal',
    },

    showModal: function(){
        $('div', '#imageModal').html('<img src="' + this.model.get('image') + '" alt="" style="padding:10px;">');
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return $(this.el);
    },
});