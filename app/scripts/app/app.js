
$(".nano").nanoScroller({ iOSNativeScrolling: false, alwaysVisible: true });

var Genie = { };

Genie.App = (function(Backbone, Marionette){

  var App = new Marionette.Application();

  App.addRegions({
    headerRegion: "header",
    footerRegion: ".footer",
    placeListRegion: ".place-list-region",
    placeDetailsRegion: ".place-details-region",
    placeMenusRegion: ".place-menus-region",
    activityListRegion: ".activity-list-region",
    placePhotosRegion: ".place-photos-region",
  });

  App.on("initialize:after", function(){
    if (Backbone.history){
      //Backbone.history.start({pushState: true});
      Backbone.history.start();
    }
  });


  App.carousel = function(selector){
    $(selector).carouFredSel({
      responsive: false,
      width: 530,
      items: 3,
      infinite: false,
      circular: false,
      auto: false,
      prev  : { 
        button  : "#prev_img",
        key   : "left"
      },
      next  : { 
        button  : "#next_img",
        key   : "right"
      },
    });  
  };

  return App;

})(Backbone, Marionette);
