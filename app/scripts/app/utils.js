Genie.Utils = {
	showAlert: function (type, msg) {
        this.isAlertShown = true;
        that = this;
        $("#alert-area").append($("<div class='alert-message alert " + type + " fade in'><p> " + msg + " </p></div>"));
        $(".alert-message").delay(2000).fadeOut("slow", function () { 
            $(this).remove();
        });
    }
};

$.fn.spin = function(opts) {
  this.each(function() {
    var $this = $(this),
        data = $this.data();

    if (data.spinner) {
      data.spinner.stop();
      delete data.spinner;
    }
    if (opts !== false) {
      data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
    }
  });
  return this;
};