$.ajaxSetup({
  xhrFields : { 
    withCredentials: true 
  },
});


Genie.Router = { };

Genie.searchQuery = '';

Genie.Router.MainRouter = Backbone.Router.extend({
  baseUrl: 'http://beta.noshgenie.com',
  routes: {
    '': 'entryPoint',
    'browse/:restaurantid': 'browseRestaurant',
    'search/:query': 'searchRestaurants',
    'about':'aboutClicked',
  },

  entryPoint: function(){
    Genie.Router.fetchPlaces();
    //Genie.Router.fetchOpinions();
    Genie.Router.initWidgets();

  },

  browseRestaurant: function (restaurantid){
    console.log("restaurantid is: " + restaurantid);
    if(!Genie.placeCollection){
      Genie.Router.router.navigate("", {trigger: true, replace: true});
      return;
    }
    Genie.App.placeDetailsRegion.on('show', function(){
      Genie.App.carousel(".thumbnails");
    });
    var r = Genie.placeCollection.get(restaurantid);
    if(r){
      var pd = new Genie.PlaceDetails.Info({ model : r });
      Genie.App.placeDetailsRegion.show(pd);
      var mv = new Genie.Menus.MenuListView({ collection: r.get('menus') });
      Genie.App.placeMenusRegion.show(mv);
      $('#my-group-list').freetile();
    }
  },

  searchRestaurants: function(query){
    if(!Genie.placeCollection){
      Genie.Router.router.navigate('', {trigger: true, replace: true});
      return;
    }
    console.log("Next URL: " + Genie.placeCollection.nextUrl);
    // Genie.placeCollection.resetMeta();
    // Genie.placeCollection.reset();
    // Genie.placeCollection.query = query;
    Genie.placeCollection.searchRequested(query);
    Genie.placeCollection.title = 'Search results for "' + query + '"';
    Genie.placeCollection.fetch({
      success: function(collection){
        //console.log("Next URL after fetch: " + collection.nextUrl);
        this.query='';
        if(collection.length < 1){
          collection.isEmpty = true;
          collection.trigger('change:isEmpty');
        }
      }
    });
  },

  aboutClicked: function(){
    console.log("About Nosh Genie");
  }
});

Genie.Router.fetchOpinions = function() {
  Genie.opinionsCollection = new Genie.Opinions.OpinionCollection();
  Genie.opinionsCollection.fetch();
  var opinions_view = new Genie.Opinions.OpinionsView({ collection: Genie.opinionsCollection });
  Genie.App.activityListRegion.show(opinions_view); 

}

Genie.Router.fetchPlaces = function(){
  Genie.placeCollection = new Genie.Places.PlaceCollection();
  Genie.placeCollection.fetch({
    success: function(collection){
      if(!collection || collection.length<1){
        return;
      }
      m = collection.at(0);
      if(m){
        v = new Genie.Places.PlaceItemView({model: m});
        m.select();
        v.showDetails();
      }
    }
  });
  var places_view = new Genie.Places.PlacesView({ collection: Genie.placeCollection });
  Genie.App.placeListRegion.show(places_view);
};

Genie.Router.initWidgets = function(){
  Genie.Widgets.init();

  var navigation = new Genie.Navigation.NavBarView();
  navigation.render();
  var searchBarView = new Genie.Navigation.SearchBarView();
  searchBarView.render();
};

Genie.App.addInitializer(function(options){
  Genie.Router.router = new Genie.Router.MainRouter();
  //Genie.App.vent.trigger("routing:started");
});