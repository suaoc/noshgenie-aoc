
Genie.Likes = { };

// Models
Genie.Likes.LikeModel = Backbone.Model.extend({
  url: function() {
    var root = this.urlRoot();
    if (this.isNew()) return root;
    return root + this.id + '/';
  },
  urlRoot: function() {
    return Genie.Router.router.baseUrl +  '/api/v1/likes/';
  },
});

