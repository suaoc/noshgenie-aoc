Genie.Widgets = {};

Genie.Widgets.init = function(){
	new Genie.Widgets.GeoLocWidget();
	//new Genie.Widgets.FBWidget().render();
}

Genie.Widgets.GeoLocWidget = Backbone.View.extend({
	el: $('#geo-loc-wgt'),

	events: {
		'click': 'findLocation',
	},

	initialize: function(){
		_.bindAll(this);
	},

	findLocation: function(){
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(this.positionSuccess, this.positionError);
		} else {
			positionError(-1);
		}
	},

	positionError: function(err) {
		var msg;
		switch(err.code) {
			case err.UNKNOWN_ERROR:
			msg = "Unable to find your location";
			break;
			case err.PERMISSION_DENINED:
			msg = "Permission denied in finding your location";
			break;
			case err.POSITION_UNAVAILABLE:
			msg = "Your location is currently unknown";
			break;
			case err.BREAK:
			msg = "Attempt to find location took too long";
			break;
			default:
			msg = "Location detection not supported in browser";
		}
		document.getElementById('info').innerHTML = msg;
	},

	positionSuccess: function (position) {
		var coords = position.coords || position.coordinate || position;
		console.log(coords.latitude + " : " + coords.longitude);
		lUrl = 'http://severe-wind-1686.herokuapp.com/api/v1/places/?format=json&limit=2&lat='+32+'&lon='+74;
		Genie.placeCollection.reset();
		Genie.placeCollection.fetch({update:true});
	}
});

Genie.Widgets.FBWidget = Backbone.View.extend({
	el: $('#fb-wgt'),
	loginViewHtml: '<span id="fb-login" class="fb-btn">facebook</span>',
	logoutViewHtml: "<span id='dp'></span><span id='logout' class='fb-btn'>logout</span>",

	events: {
		'click #fb-login': 'loginToFb',
		'click #logout': 'logout',
	},

	initialize: function(){
		_.bindAll(this,'render');
	},

	render: function(){
    console.log('rendering facebook widget');
    if(Genie.Auth.loggedIn){
      this.showLogoutView();
    }else{
      this.showLoginView();
    }
	},

	showLoginView: function(){
		$(this.el).html(this.loginViewHtml);
	},
	
  showLogoutView: function(response){
    $(this.el).html(this.logoutViewHtml);
    FB.api('/me', function(response) {
     $('#dp', $('#fb-wgt')).html('<img width="24" height="24" src="http://graph.facebook.com/' + response.id + '/picture" />');
   });		
  },

  logout: function(){
    FB.logout(function(response){
     console.log('logged out' + console.response);
   });
    this.showLoginView();
  },

  loginToFb: function(){
    var view = this;
    var lh = function(response) {
      if (response.authResponse) {
        // connected
        Genie.Auth.loginToGenie(response.authResponse.accessToken);
        view.showLogoutView(response);
      } else {
        // cancelled
      }
  	};
    Genie.Auth.checkLogin(function(response) {
    	if (response.status === 'connected') {
        // connected
        console.log("already logged in");
        view.showLogoutView(response);
      } else if (response.status === 'not_authorized') {
        // not_authorized
      } else {
        // not_logged_in
        console.log('not logged in');
        FB.login(lh);
      }
		});
	},
});