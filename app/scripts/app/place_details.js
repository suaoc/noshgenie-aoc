Genie.PlaceDetails = { };

Genie.PlaceDetails.Info = Backbone.View.extend({
  tagName: 'div',
  className: 'place-details',
  template: _.template(
      "<div id='details-alert'></div>\
      <div class='place-info'>\
        <div class='restaurant-name'><%= restaurant_name%></div>\
        <div class='checkins'>\
            <span class='checkins-count'><%= checkins %> checkins</span>\
            <span class='checkin-button'>\
                <span class='icon'></span>\
                <span class='title'>Checkin</span>\
            </span>\
        </div>\
        <div class='directions'>\
            <span class='show-directions-button'>\
                <span class='title'>Directions</span>\
            </span>\
        </div>\
        <% if (brief_description) { %>\
          <div class='description'> Description: <%= brief_description %></div>\
        <% } %>\
      </div>\
      <div id='inner-gallery'></div>\
      <div class='opinions'>\
          <div class='opinions-title'>Reviews</div>\
          <div class='opinions-list'></div>\
          <div class='add-opinion'></div>\
      </div>\
      <div class='menus'>\
          <div class='menus-title'>Menus</div>\
      </div>\
      "
    ),
  ratingTplStr: "<% for (var i = 1; i <= 5; i++) {\
                    if (i <= rating) { %>\
                      <i class='icon-star'></i>\
                    <%} else { %>\
                      <i class='icon-star-empty'></i>\
                    <% } %>\
                  <% };%>",
  addOpinionStr: "<div class='opinion-item' id='post-opinion'>\
                    <div id='raty-field'></div>\
                    <textarea class='comment-field' placeholder='What do you think about this place?'></textarea>\
                    <span class='review-button'>\
                        <span class='icon'></span>\
                        <span class='title'>Add Review</span>\
                    </span><br/><br/>\
                  </div>",
  loginToFbMsg: "<div class='opinion-item'>Please <a class='login-link'>login</a> to facebook to post a comment.</div>",

  initialize: function(){
    console.log('initializing place details');
    _.bindAll(this, 'render', 'checkin', 'incrementCheckinCount', 'showNewReview', 'addReview', 'showDirections');
    this.model.on('change:reRender', this.updateDetails,this);
  },
  
  events: {
     "click .checkin-button": "checkin",
     "click .review-button": "addReview",
     "click .show-directions-button": "showDirections",
     'click .login-link': 'callFbWidget',
  },
  updateDetails: function(){
    if(this.model.get('reRender')){
      this.updatePostReviewBox();
      this.model.set({'reRender':false}); // reset reRender to false
    }
  },

  updatePostReviewBox: function(){
    view = this;
    if(typeof(FB) != undefined && FB!=null){
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          view.$('.add-opinion').html(view.addOpinionStr);
        } else if (response.status === 'not_authorized') {
          // not authorized
        } else {
          view.$('.add-opinion').html(view.loginToFbMsg);
        }
      });
    }
    this.$('#raty-field').raty({
      half: true,
      scoreName: 'raty-score',
    });
  },

  render: function() {
    $(this.el).append(this.template(this.model.toJSON()));

    this.updatePostReviewBox();

    if (this.model.get('latitude') && this.model.get('longitude')) {
        this.$('.directions').show();
    }
    
    var opinions_collection = this.model.get('opinions');
    var opinions_view = new Genie.Opinions.OpinionsView({ collection: opinions_collection });
    this.$('.opinions-list').html(opinions_view.render());

    var menus_collection = this.model.get('menus');
    if (menus_collection) {
      var menus_view = new Genie.Menus.MenuListView({ collection: menus_collection });
      this.$('.menus').append(menus_view.render());
    }

    var gallery = this.model.get('photos');
    if(gallery.length>0){
      console.log("gallery length: " + gallery.length);
      var pv = new Genie.Photos.PhotosView({ collection: gallery });
      this.$('#inner-gallery').html(pv.render());
    }

    return $(this.el);
  },
  
  incrementCheckinCount: function() {
      var checkin_count = this.model.get('checkins') || 0;
      var new_checkin_count = checkin_count + 1;
      this.$('.checkins-count').html(new_checkin_count + ' checkins');
  },
  
  checkin: function() {
    if(!Genie.Auth.loggedIn){
      this.$('#details-alert').html('Sign in to facebook.').addClass('alert');
      setTimeout(function(){this.$('#details-alert').html('').removeClass('alert');}, 2000);
      return;
    }
      var view = this;
      var done = false;
      this.$('.checkin-button .icon').animate({
          'min-width': '20px',
      }, 50, function() {
         if (!done) {
             view.$('.checkin-button .icon').spin({'length': 3, 'lines': 7, 'radius': 3, 'width': 3,});
         }
      });
      
      var checkin = new Genie.Checkins.CheckinModel({ place: this.model.get('resource_uri') });
      checkin.save({}, {
          error: function(model, response) {
              done = true;
              view.$('.checkin-button .icon').spin(false);
              view.$('.checkin-button .icon').css({ 'min-width': '0px' })
              view.$('.checkin-button .title').html('Checkin');
          },
          success: function(model, response) {
              done: true;
              view.$('.checkin-button .icon').spin(false);
              view.$('.checkin-button .icon').css({ 'min-width': '0px' })
              view.$('.checkin-button .icon').html('✓');
              view.$('.checkin-button .title').html('Checked In');
              view.incrementCheckinCount();
          },
      });
  },
  
  showNewReview: function(model) {
    opinion_view = new Genie.Opinions.OpinionItemView({model: model});
    var rev = $(opinion_view.render()).hide();
    this.$('.opinions-list').append(rev);
    item.fadeIn(rev);
  },

  addReview: function() {
    if(!Genie.Auth.loggedIn){
      console.log('Only logged in users can post a review!');
      return;
    }
    var rated = this.$('[name=raty-score]').val();
    var commented = this.$('.comment-field').val();
    if(!rated){
      if(!this.$('#post-opinion').find(".alert").length > 0){
        this.$('#post-opinion').append("<div class='alert'>Rating can not be empty!</div>");
        setTimeout(function() { this.$('.alert', '#post-opinion').remove(); }, 1000);
      }
      return;
    }
    if(!commented){
      if(!this.$('#post-opinion').find(".alert").length > 0){
        this.$('#post-opinion').append("<div class='alert'>Comment can not be empty!</div>");
        setTimeout(function() { this.$('.alert', '#post-opinion').remove(); }, 1000);
      }
      return;
    }

    var view = this;
    var done = false;
    this.$('.review-button .icon').animate({
        'min-width': '20px',
    }, 50, function() {
       if (!done) {
           view.$('.review-button .icon').spin({'length': 3, 'lines': 7, 'radius': 3, 'width': 3,});
           view.$('.review-button .title').html('Saving...');
       }
    });
    
    var opinion = new Genie.Opinions.OpinionModel({
        place: this.model.get('resource_uri'),
        rating: rated,
        comment: commented,      
      });
      
    opinion.save({}, {
        error: function(model, response) {
            done = true;
            view.$('.review-button .icon').spin(false);
            view.$('.review-button .icon').css({ 'min-width': '0px' })
            view.$('.review-button .title').html('Add Review');
        },
        success: function(model, response) {
            done = true;
            view.$('.review-button .icon').spin(false);
            view.$('.review-button .icon').css({ 'min-width': '0px' })
            //view.$('.review-button .icon').html('✓');
            //view.$('.review-button .title').html('Review Added');
            view.$('.review-button .title').html('Add Review');
            if(!this.$('#post-opinion').find(".alert").length > 0){
              this.$('#post-opinion').append("<div class='alert'>✓ Review posted successfully!</div>");
              setTimeout(function() { this.$('.alert', '#post-opinion').remove(); }, 1000);
            }            
            view.showNewReview(model);
        },
    });

    // reset post review box
    this.updatePostReviewBox();
  },
  
  showDirections: function() {
      var source_coordinates = '';
      var destination_coordinates = this.model.get('latitude') + ','  + this.model.get('longitude');
      var googleDirectionsLink = 'http://maps.google.com/?saddr=' + source_coordinates + '&daddr=' + destination_coordinates;
      window.open(googleDirectionsLink);
  },
  callFbWidget: function(){
    if(Genie.fbWidget){
      Genie.fbWidget.loginToFb();
    }
  },
});
